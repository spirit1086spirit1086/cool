<?php

namespace App\Http\Controllers;

use App\Http\Resources\Mail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\Mail as MailResource;
use App\Http\Models;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail as SendMail;
use Illuminate\Support\Str;


class MailController extends Controller
{
    /**
     * Method get api param, validate model, if all success
     * call notification method
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
    */
    public function send(Request $request)
    {
        $uid =  Cookie::get('mail_api_key');

        $is_have_uid = Models\Mail::getUidMessageCount($uid);

        $response_code = 200;
        $message = addslashes($request->input('message'));

        $mail = new Models\Mail();

            // model validate
            $validator = Validator::make($request->all(), $mail->rules(), $mail->messages());

            if ($validator->fails()) {
                $mail->errors = json_encode($validator->errors());
                $response_code = 400;
            }
            elseif($is_have_uid==0)
            {
                $uid = Str::uuid();
                // send message
                $notification_response = $this->setNotification($message);
                //saved to table mails
                $mail->setInfo($message, $notification_response, $uid);
                $response_code = $notification_response ? 200 : 400;
            }
            else
           {
              $response_code = 400;
              $mail->errors = trans('notification.user_already_send');
           }

        $mail->status = $response_code;
        // api response
        $resource = new MailResource($mail);
        return $resource->response()->cookie('mail_api_key', $uid)->setStatusCode($response_code);
    }

    /**
     * notification method
     * @param string $message
     * @return bool
    */
    private function setNotification($message)
    {
        $status = false;

       try
       {
           SendMail::raw($message, function($m)
           {
               $m->from('support@domain.local', 'Laravel')
                 ->to('fixed_email@example.com');
           });

           if (SendMail::failures())
           {
               Log::info('Failed mails: '.json_encode(SendMail::failures()));
           }
           else
           {
               $status = true;
           }
        }
        catch(\Swift_TransportException $transportExp)
        {
            Log::info('Failed swift: '.$transportExp->getMessage());
        }
        return $status;
    }


}

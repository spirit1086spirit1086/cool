<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    /**
     * method render main page
     * @return \Illuminate\View\View
    */
    public function index()
    {
        return view('welcome');
    }

    /**
     * method set curl and set responsed cookie to client browser
     *  @param  \Illuminate\Http\Request  $request
     *  @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    public function mailing(Request $request)
    {
        $uid =  Cookie::get('mail_api_key');
        $message = $request->input('message');

        $data = ['message' => $message];
        $url = 'http://localhost/api/send';

        $result = $this->curl($data,$url,$uid);

        // set cookie to header client
        return redirect()->back()
                         ->withCookie(cookie()->forever('mail_api_key', $result->cookie['mail_api_key']))
                         ->with('message', $result->message_response);
    }

    /**
     * Method send message to Mail api
     * @param array $data
     * @param string $url
     * @param string $uid
     * @return mixed
     */

    private function curl($data,$url,$uid)
    {
        $params = json_encode($data);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($params),
                "Cookie: mail_api_key=".$uid)
        );
        curl_setopt($ch, CURLOPT_HEADER, 1);

        $response = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        curl_close($ch);

        // get response headers
        $header = substr($response, 0, $header_size);
        // get response body
        $body = substr($response, $header_size);

        if ($http_code == 200) {
          $message_response = trans('notification.success');
        } else {
          $data = json_decode($body);
          $message_response = $data->data->errors;
       }

        // get response cookies
       $cookie = $this->parseHeader($header);

       $result = new \stdClass();
       $result->cookie = $cookie;
       $result->message_response = $message_response;

      return $result;
    }

    /**
     * method parse header
     * @param  string $result
     * @return array
    */
    private  function parseHeader($result)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);
        $cookies = array();
        foreach($matches[1] as $item) {
            parse_str($item, $cookie);
            $cookies = array_merge($cookies, $cookie);
        }

        return $cookies;
    }




}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Mail extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $response=[];
        if($this->status==400)
        {
            $response['errors'] = $this->errors ? $this->errors : trans('notification.send_error');
        }
        else
        {
            $response['response'] = 'success';
        }

        return $response;
    }
}

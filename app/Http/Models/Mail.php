<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Mail extends Model
{
    /**
     * method checked model rules
     * @return array
    */
    public function rules()
    {
        return  [
            'message'=>'required'
        ];
    }

    /**
     * method return messages of validate model
     * @return array
    */
    public function messages()
    {
        return [
            'required' => trans('validation.required')
        ];
    }

    /**
     * method saved data in model
     * @return void
     */
    public function setInfo($message,$notification_response,$uid)
    {
        $mail = new Mail();
        $mail->message = $message;
        $mail->is_success = $notification_response ? 1 : null;
        $mail->is_error = !$notification_response ? 1 : null;
        $mail->uid = $uid;
        $mail->save();
    }

    /**
     * method get count uid
     * @param string $uid
     * @return int
    */
    public  static function getUidMessageCount($uid)
    {
        return Mail::where('uid',$uid)->count();
    }
}

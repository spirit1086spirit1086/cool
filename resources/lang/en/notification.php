<?php

return [
    'send_error' => 'unexpected error occurred while sending the message',
    'success'=>'send success',
    'error'=>'send error',
    'user_already_send'=>'user already send message',
    'user_not_found'=>'user not found'
];

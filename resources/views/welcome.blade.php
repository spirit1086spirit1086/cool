@extends('layouts.app')

@section('content')
            <div class="content">
                @if (\Session::has('message'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('message') !!}</li>
                        </ul>
                    </div>
                @endif
                <form action="{{route('mailing')}}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                       <textarea name="message" rows="5" cols="30" required></textarea>
                    </div>
                    <div class="form-group">
                       <input type="submit" value="send">
                    </div>
                </form>

            </div>
@endsection
